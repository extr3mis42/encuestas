/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proj.encuestas.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author jonad
 */


@Entity
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {

      @Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
      private Long id;
      @NotBlank(message ="El nombre es requerido")
      private String firstName;
      @NotBlank(message ="El apellido es requerido")
      private String lastName;
      @NotBlank(message ="El email es requerido")
      private String email;
      @CreatedDate
      private Date lastLogin;
      private Boolean isActive;
      @CreatedDate
      private Date dateJoined;
      @NotBlank(message ="El password es requerido")
      private String password;
      @NotBlank(message ="El nombre de usuario es requerido")
      private String userName;
      private Boolean isAdmin;
      private Date birthDate;

      public User() {
      }

      public User(Long id, String firstName, String lastName, String email, Date lastLogin, Boolean isActive, Date dateJoined, String password, String userName, Boolean isAdmin, Date birthDate) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.lastLogin = lastLogin;
            this.isActive = isActive;
            this.dateJoined = dateJoined;
            this.password = password;
            this.userName = userName;
            this.isAdmin = isAdmin;
            this.birthDate= birthDate;
      }

      
      public Long getId() {
            return id;
      }

      public void setId(Long id) {
            this.id = id;
      }

      public String getFirstName() {
            return firstName;
      }

      public void setFirstName(String firstName) {
            this.firstName = firstName;
      }

      public String getLastName() {
            return lastName;
      }

      public void setLastName(String lastName) {
            this.lastName = lastName;
      }

      public String getEmail() {
            return email;
      }

      public void setEmail(String email) {
            this.email = email;
      }

      public Date getLastLogin() {
            return lastLogin;
      }

      public void setLastLogin(Date lastLogin) {
            this.lastLogin = lastLogin;
      }

      public Boolean getIsActive() {
            return isActive;
      }

      public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
      }

      public Date getDateJoined() {
            return dateJoined;
      }

      public void setDateJoined(Date dateJoined) {
            this.dateJoined = dateJoined;
      }

      public String getPassword() {
            return password;
      }

      public void setPassword(String password) {
            this.password = password;
      }

      public String getUserName() {
            return userName;
      }

      public void setUserName(String userName) {
            this.userName = userName;
      }

      public Boolean getIsAdmin() {
            return isAdmin;
      }

      public void setIsAdmin(Boolean isAdmin) {
            this.isAdmin = isAdmin;
      }
   
      public Date getBirhDate() {
            return birthDate;
      }

      public void setBirhDate(Date birhDate) {
            this.birthDate = birhDate;
      }

      
      
}
