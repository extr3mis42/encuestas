/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proj.encuestas.controller;

import com.proj.encuestas.library.Encrypt;
import com.proj.encuestas.model.Login;
import com.proj.encuestas.model.User;
import com.proj.encuestas.repository.UserRepository;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author jonad
 */
@Controller
public class AuthController {

      @Autowired
      public UserRepository userRepository;

      @GetMapping(value = {"/signup"})
      public String signup(Model model) {
            model.addAttribute("user", new User());
            return "signup";
      }

      @PostMapping(value = {"/signup"})
      @Transactional
      public String signup(@Valid User user, BindingResult result, Model model, HttpSession session) throws Exception {
            if (result.hasErrors()) {
                  model.addAttribute("user", user);
                  return "signup";
            } else {
                  List<User> userData = userRepository.findAll().stream()
                          .filter(u -> u.getEmail().equals(user.getEmail()))
                          .collect(Collectors.toList());
                  if (userData.isEmpty()) {

                        Date utilDate = new Date(); //fecha actual
                        user.setIsActive(Boolean.TRUE);
                        user.setIsAdmin(Boolean.FALSE);
                        user.setPassword(Encrypt.encrypt(user.getPassword()));
                        user.setDateJoined(utilDate);
                        user.setLastLogin(utilDate);
                        user.setBirhDate(utilDate);
                        userRepository.save(user);
                        Object[] data = {
                              user.getEmail(),
                              user.getFirstName(),
                              user.getIsAdmin(),
                              user.getLastName(),
                              user.getUserName()
                        };
                        System.out.println("datos" + Arrays.toString(data));
                        session.setAttribute("usersession", Arrays.toString(data));
                        return "redirect:/main";
                  } else {
                        result.rejectValue("email", "error.user", "An account already exists for this email");
                        model.addAttribute("user", user);
                        return "signup";
                  }
            }
      }

      @GetMapping(value = {"/login"})
      public String login(Model model) {
            model.addAttribute("login", new Login());
            return "login";
      }

      @PostMapping(value = {"/login"})
      @Transactional
      public String login(@Valid Login login, BindingResult result, Model model, HttpSession session) throws Exception {
            if (result.hasErrors()) {
                  model.addAttribute("login", login);
                  return "login";
            } else {
                  List<User> userData = userRepository.findAll().stream()
                          .filter(u -> u.getEmail().equals(login.getEmail()))
                          .collect(Collectors.toList());
                  if (userData.isEmpty()) {
                        result.rejectValue("email", "error.user", "Wrong credentials");
                        model.addAttribute("user", login);
                        return "login";
                  }else {
                        
                  }
            }
            return "login";

      }
}
